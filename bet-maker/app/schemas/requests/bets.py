from decimal import Decimal

from pydantic import UUID4, BaseModel, Field


class BetCreateRequest(BaseModel):
    amount: float = Field(gt=0)
    event_id: UUID4


class BetRequest(BetCreateRequest):
    odds: Decimal
