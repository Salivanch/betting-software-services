from enum import StrEnum


class BetStatus(StrEnum):
    PENDING = "PENDING"
    WON = "WON"
    LOST = "LOST"
    CANCELED = "CANCELED"
