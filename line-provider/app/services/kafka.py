import json
import logging

from aiokafka import AIOKafkaProducer
from core.config import config

logger = logging.getLogger(__name__)


class KafkaProducerService:
    def __init__(self):
        self.producer = None

    async def start(self):
        self.producer = AIOKafkaProducer(
            bootstrap_servers=config.KAFKA_SERVERS,
            value_serializer=lambda x: json.dumps(x).encode(),
            key_serializer=lambda x: str(x).encode(),
        )
        await self.producer.start()

    async def stop(self):
        await self.producer.stop()

    async def send(self, topic, value=None, key=None):
        await self.producer.send(topic, value=value, key=key)

    async def health_check(self) -> bool:
        try:
            await self.producer.send_and_wait("test_topic", "test_message")
            return True
        except Exception as e:
            logging.error(
                f"Error while trying to health check kafka. Exception: {e}",
                exc_info=True,
            )
            return False


kafka_service = KafkaProducerService()
