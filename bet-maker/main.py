import uvicorn
from core.config import config

if __name__ == "__main__":
    uvicorn.run(
        app="core.server:app",
        reload=config.ENVIRONMENT != "production",
        workers=config.WORKERS,
        host="0.0.0.0",
    )
