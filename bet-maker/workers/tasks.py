import logging

from app.models.bets import Bet
from app.repositories import BetRepository
from workers import celery_app, sync

from modules.database.postgres import (session, set_session_context,
                                       sync_session)
from modules.database.postgres.session import reset_session_context

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)


@celery_app.task
@sync
async def calculated_bets(event_id: str, status: str) -> None:
    """
    Celery задача для расчета ставок по event_id.
    """
    context = set_session_context(str(event_id))
    try:
        async with session() as session_:
            bet_repository = BetRepository(Bet, session_)

            await bet_repository.calculate_results_by_event_id(event_id, status)

    except Exception as e:
        logger.exception(
            f"Failed to update bets from event:{event_id}.\n{e}", exc_info=e
        )
    finally:
        sync_session.remove()
        reset_session_context(context=context)
