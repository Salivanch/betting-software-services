from datetime import datetime
from decimal import Decimal

from app.models.events.choices import EventStatus
from pydantic import UUID4, BaseModel


class EventRequest(BaseModel):
    id: UUID4
    status: EventStatus
    odds: Decimal
    deadline: datetime
