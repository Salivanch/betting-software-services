from app.models.matches import Match
from app.repositories import MatchRepository

from modules.controller import BaseController


class MatchController(BaseController[Match]):
    def __init__(self, match_repository: MatchRepository) -> None:
        super().__init__(model=Match, repository=match_repository)
