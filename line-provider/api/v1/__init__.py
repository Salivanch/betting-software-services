from fastapi import APIRouter

from .events import events_router
from .matches import matches_router

v1_router = APIRouter()
v1_router.include_router(events_router)
v1_router.include_router(matches_router)
