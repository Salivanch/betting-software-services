from app.controllers import MatchController
from app.schemas.requests.base import Filter, OrderEnum
from app.schemas.requests.matches import MatchCreateRequest
from app.schemas.response_schemas import PaginationResponse
from app.schemas.responses.matches import MatchResponse
from core.factory import Factory
from fastapi import APIRouter, Depends

from modules.utils.parsers import parse_q

match_router = APIRouter()


@match_router.get("/", name="Получить список матчей")
async def get_list(
    match_controller: MatchController = Depends(Factory().get_match_controller),
    q: list[Filter] = Depends(parse_q),
    page: int = 1,
    limit: int = 10,
    sort_by: str = None,
    order: OrderEnum = OrderEnum.asc,
) -> PaginationResponse[MatchResponse]:
    results = await match_controller.get_by(
        page=page,
        limit=limit,
        filter_list=q,
        sort_by=sort_by,
        order=sort_by,
        pydantic_model=MatchResponse,
    )
    found = await match_controller.count()
    return PaginationResponse[MatchResponse](
        meta={"found": found, "page": page, "limit": limit}, items=results
    )


@match_router.post("/", name="Создать матч")
async def create(
    match_create: MatchCreateRequest,
    match_controller: MatchController = Depends(Factory().get_match_controller),
) -> MatchResponse:
    new_match = match_create.model_dump()
    created_match_data = await match_controller.create(new_match)
    return created_match_data
