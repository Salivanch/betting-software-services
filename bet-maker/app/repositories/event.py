from app.models.events import Event

from modules.repository import BaseRepository


class EventRepository(BaseRepository[Event]):
    pass
