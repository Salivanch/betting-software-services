from app.controllers import EventController
from app.schemas.requests.base import Filter, OrderEnum
from app.schemas.response_schemas import PaginationResponse
from app.schemas.responses.events import EventResponse
from core.factory import Factory
from fastapi import APIRouter, Depends

from modules.utils.parsers import parse_q

event_router = APIRouter()


@event_router.get("/", name="Получить список событий")
async def get_list(
    event_controller: EventController = Depends(Factory().get_event_controller),
    q: list[Filter] = Depends(parse_q),
    page: int = 1,
    limit: int = 10,
    sort_by: str = None,
    order: OrderEnum = OrderEnum.asc,
) -> PaginationResponse[EventResponse]:
    results = await event_controller.get_by(
        page=page,
        limit=limit,
        filter_list=q,
        sort_by=sort_by,
        order=sort_by,
        pydantic_model=EventResponse,
    )
    found = await event_controller.count()
    return PaginationResponse[EventResponse](
        meta={"found": found, "page": page, "limit": limit}, items=results
    )
