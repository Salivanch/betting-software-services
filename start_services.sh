#!/bin/bash

# Переменная для хранения имени сети
NETWORK_NAME=kafka_betting_network

# Проверяем, существует ли уже сеть
network_exists=$(docker network ls | grep $NETWORK_NAME | awk '{print $2}')
if [ -z "$network_exists" ]; then
  # Создаем сеть, если она не существует
  echo "Creating Docker network: $NETWORK_NAME"
  docker network create $NETWORK_NAME
else
  echo "Docker network $NETWORK_NAME already exists"
fi

# Запуск сервисов
echo "Starting Kafka..."
cd kafka
docker-compose up -build
cd ..

echo "Starting bet-maker..."
cd bet-maker
docker-compose up -build
cd ..

echo "Starting line-provider..."
cd line-provider
docker-compose up -build
cd ..

echo "All services are started."
