import json
import logging

from aiokafka import AIOKafkaConsumer
from core.config import config

from .message_handlers import handle_create_event, handle_update_event

logger = logging.getLogger(__name__)


class KafkaConsumerService:
    def __init__(self):
        self.consumer = None

    async def start(self):
        self.consumer = AIOKafkaConsumer(
            "create_event",
            "update_event",
            bootstrap_servers=config.KAFKA_SERVERS,
            group_id="bet_group",
            enable_auto_commit=False,
            value_deserializer=lambda m: json.loads(m.decode("utf-8")),
        )
        await self.consumer.start()
        logger.info("Kafka consumer started")

    async def stop(self):
        await self.consumer.stop()
        logger.info("Kafka consumer stopped")

    async def consume(self):
        try:
            async for message in self.consumer:
                logger.info(
                    f"Received message from topic {message.topic}: {message.value}"
                )

                try:
                    message_data = json.loads(message.value)

                    if message.topic == "create_event":
                        await handle_create_event(message_data)
                    elif message.topic == "update_event":
                        await handle_update_event(message_data)

                    # Если обработка прошла успешно, подтверждаем обработку сообщения
                    await self.consumer.commit()

                except Exception as e:
                    logger.error(f"Error processing message: {e}")
                    # В случае ошибки, реализуем Dead Letter Queue

        except Exception as e:
            logger.error(f"Error in consuming messages: {e}", exc_info=True)


kafka_service = KafkaConsumerService()
