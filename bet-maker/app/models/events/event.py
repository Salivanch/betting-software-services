import uuid

from sqlalchemy import Column, DateTime, Enum, Numeric
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import relationship

from modules.database.postgres import Base

from .choices import EventStatus


class Event(Base):
    __tablename__ = "events"

    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    status = Column(Enum(EventStatus), default=EventStatus.PENDING)
    odds = Column(Numeric(5, 2), nullable=False)
    deadline = Column(DateTime)

    bets = relationship("Bet", back_populates="event")
