from enum import StrEnum


class ResultEventStatus(StrEnum):
    WON = "WON"
    LOST = "LOST"


class EventStatus(StrEnum):
    LIVE = "LIVE"
    PENDING = "PENDING"
    CANCELED = "CANCELED"
    WON = "WON"
    LOST = "LOST"
