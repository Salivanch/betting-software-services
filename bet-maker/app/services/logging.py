from app.models.logs import Log
from app.repositories.logs import LogRepository


class LoggingService:
    def __init__(self, log_repository: LogRepository):
        self.log_repository = log_repository

    async def log_action(self, attributes: dict) -> Log:
        return await self.log_repository.create(attributes)

    async def update_log(self, log: Log):
        return await self.log_repository.update(log)
