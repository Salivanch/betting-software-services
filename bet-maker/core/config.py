import json
import os
from enum import Enum
from functools import lru_cache

from pydantic_settings import BaseSettings, SettingsConfigDict


class EnvironmentType(str, Enum):
    DEVELOPMENT = "development"
    PRODUCTION = "production"
    LOCAL = "local"


class BaseConfig(BaseSettings):
    model_config = SettingsConfigDict(case_sensitive=True)


class Config(BaseConfig):
    # Environment
    DEBUG: int = 0
    DEFAULT_LOCALE: str = "en_US"
    ENVIRONMENT: str = EnvironmentType.DEVELOPMENT
    RELEASE_VERSION: str = "0.1"
    SHOW_SQL_ALCHEMY_QUERIES: int = 0
    WORKERS: int = 4
    IS_DOCKER: bool = True
    LOG_DIR: str = "./logs"

    # PostgreSQL
    POSTGRES_URL: str = (
        "postgresql+asyncpg://postgres:password123@postgresql-service:5432/bet-maker-db"
    )
    POSTGRES_URL_LOCAL: str = (
        "postgresql+asyncpg://postgres:password123@localhost:5435/bet-maker-db"
    )
    POSTGRES_URL_SYNC: str = (
        "postgresql://postgres:password123@localhost:5435/bet-maker-db"
    )

    # Redis
    REDIS_TTL: int = 300
    REDIS_URL: str = "redis://redis:6379/7"
    CELERY_BACKEND_URL: str = "redis://redis:6379/8"
    CELERY_BROKER_URL: str = "redis://redis:6379"

    # Kafka
    KAFKA_SERVERS: str = "kafka:9092"

    @classmethod
    def from_json(cls, json_file: str) -> "Config":
        try:
            with open(json_file) as f:
                config_dict = json.load(f)
        except FileNotFoundError:
            config_dict = {
                k: v for k, v in os.environ.items() if k in cls.__annotations__
            }
        return cls(**config_dict)


def ensure_directory_exists(directories: list[str]) -> None:
    for directory in directories:
        os.makedirs(directory, exist_ok=True)


@lru_cache
def get_settings() -> Config:
    configuration = Config.from_json("config.json")
    ensure_directory_exists([configuration.LOG_DIR])
    return configuration


config: Config = get_settings()
