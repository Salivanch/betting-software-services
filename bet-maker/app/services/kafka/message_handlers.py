import logging

from app.schemas.requests.events import EventRequest
from core.utils import get_event_controller

from modules.database.postgres import session

logger = logging.getLogger(__name__)


async def handle_create_event(message: dict) -> None:
    async with session() as session_:
        event_controller = await get_event_controller(session_)
        await event_controller.create(pydantic_model=EventRequest(**message))


async def handle_update_event(message: dict) -> None:
    async with session() as session_:
        event_controller = await get_event_controller(session_)
        await event_controller.update_event(EventRequest(**message))
