from .kafka import kafka_service

__all__ = [
    "kafka_service",
]
