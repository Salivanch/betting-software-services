from datetime import datetime

from app.models.bets import Bet
from app.models.bets.constants import (BETTING_TIME_EXPIRED,
                                       EVENT_CLOSED_FOR_BETS)
from app.models.events.constants import EVENT_TO_BET
from app.repositories import BetRepository
from app.schemas.requests.bets import BetCreateRequest, BetRequest
from core.utils import get_event_controller

from modules.controller import BaseController
from modules.database.postgres import session
from modules.exceptions import BadRequestException


class BetController(BaseController[Bet]):
    def __init__(self, bet_repository: BetRepository) -> None:
        super().__init__(model=Bet, repository=bet_repository)

    async def make_bet(self, bet_data: BetCreateRequest) -> Bet:
        async with session() as session_:
            event_controller = await get_event_controller(session_)
            event = await event_controller.get_by_id(bet_data.event_id)

            if event.status not in EVENT_TO_BET:
                raise BadRequestException(EVENT_CLOSED_FOR_BETS)

            if event.deadline < datetime.now():
                raise BadRequestException(BETTING_TIME_EXPIRED)

            return await self.create(
                pydantic_model=BetRequest(**bet_data.dict(), odds=event.odds)
            )
