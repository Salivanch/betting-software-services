from app.models.bets import Bet
from app.models.bets.constants import BET_TO_CALCULATED
from app.models.events.choices import EventStatus
from sqlalchemy import case, func, update

from modules.repository import BaseRepository


class BetRepository(BaseRepository[Bet]):
    async def calculate_results_by_event_id(self, event_id: str, status: str) -> None:
        """
        Рассчитывает результаты для всех ставок, связанных с указанным event_id.
        """
        await self.session.execute(
            update(Bet)
            .where(
                Bet.event_id == event_id,
                Bet.status.in_(BET_TO_CALCULATED),
            )
            .values(
                result=func.round(
                    case(
                        (status == EventStatus.WON, Bet.amount * Bet.odds),
                        (status == EventStatus.LOST, -Bet.amount),
                        else_=0,
                    ),
                    2,
                ),
                is_calculated=True,
                status=status,
            )
        )
        await self.session.commit()
