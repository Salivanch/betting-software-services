from fastapi import APIRouter

from .matches import match_router

matches_router = APIRouter()
matches_router.include_router(match_router, prefix="/matches", tags=["Matchs"])
