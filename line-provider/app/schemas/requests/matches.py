from pydantic import BaseModel


class MatchCreateRequest(BaseModel):
    name: str
