from .choices import EventStatus

EVENT_TO_BET = [EventStatus.LIVE, EventStatus.PENDING]
EVENT_TO_CALCULATED = [EventStatus.WON, EventStatus.LOST, EventStatus.CANCELED]
