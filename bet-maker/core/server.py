import asyncio
import sys
import traceback
from contextlib import asynccontextmanager

from api import router
from app.services import kafka_service
from core.config import config
from fastapi import Depends, FastAPI, Request
from fastapi.middleware import Middleware
from fastapi.middleware.cors import CORSMiddleware
from fastapi.responses import JSONResponse
from fastapi.security import HTTPBearer

from modules.exceptions import CustomException
from modules.fastapi.dependencies import Logging
from modules.fastapi.middlewares import SQLAlchemyMiddleware


def init_routers(app_: FastAPI) -> None:
    app_.include_router(router)


def init_listeners(app_: FastAPI) -> None:
    @app_.exception_handler(CustomException)
    async def custom_exception_handler(request: Request, exc: CustomException):
        request.state.exception = exc
        request.state.stack = "".join(traceback.format_exception(*sys.exc_info()))
        return JSONResponse(
            status_code=exc.code,
            content={"error_code": exc.error_code, "message": exc.message},
            headers=exc.headers or None,
        )


def make_middleware() -> list[Middleware]:
    return [
        Middleware(
            CORSMiddleware,
            allow_origins=["*"],
            allow_credentials=True,
            allow_methods=["GET", "POST", "PUT", "OPTIONS", "DELETE", "PATCH"],
            allow_headers=[
                "Accept",
                "Authorization",
                "Origin",
                "DNT",
                "X-CustomHeader",
                "Keep-Alive",
                "User-Agent",
                "X-Requested-With",
                "If-Modified-Since",
                "Cache-Control",
                "Content-Type",
                "Content-Range",
                "Range",
            ],
        ),
        Middleware(SQLAlchemyMiddleware),
        # Middleware(LoggingMiddleware),
    ]


@asynccontextmanager
async def lifespan(app: FastAPI):
    await kafka_service.start()
    consumer_task = asyncio.create_task(kafka_service.consume())
    try:
        yield
    finally:
        consumer_task.cancel()
        try:
            await consumer_task
        except asyncio.CancelledError:
            pass
        await kafka_service.stop()


def create_app() -> FastAPI:
    http_bearer = HTTPBearer(auto_error=False)
    app_ = FastAPI(
        title="backend-service",
        description="Fast API backend service",
        version="1.0.0",
        docs_url=None if config.ENVIRONMENT == "production" else "/docs",
        redoc_url=None if config.ENVIRONMENT == "production" else "/redoc",
        openapi_url=(
            None
            if config.ENVIRONMENT == "production"
            else (
                "/bet-maker/openapi"
                if config.ENVIRONMENT == "development"
                else "/openapi"
            )
        ),
        root_path=(
            "/bet-maker" if config.ENVIRONMENT in ["development", "production"] else "/"
        ),
        dependencies=[Depends(Logging), Depends(http_bearer)],
        middleware=make_middleware(),
        debug=config.ENVIRONMENT != "production",
        swagger_ui_parameters={
            "defaultModelsExpandDepth": 5,
            "displayModelExpandDepth": 5,
            "displayRequestDuration": True,
        },
        lifespan=lifespan,
    )
    init_routers(app_=app_)
    init_listeners(app_=app_)

    return app_


app = create_app()
