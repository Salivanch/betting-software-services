from enum import StrEnum


class EventStatus(StrEnum):
    LIVE = "LIVE"
    PENDING = "PENDING"
    CANCELED = "CANCELED"
    WON = "WON"
    LOST = "LOST"
