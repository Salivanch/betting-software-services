from uuid import UUID

from app.schemas.requests.matches import MatchCreateRequest


class MatchResponse(MatchCreateRequest):
    id: UUID
