import asyncio
import functools

from celery import Celery
from core.config import config

celery_app = Celery(
    "worker",
    backend=config.CELERY_BACKEND_URL,
    broker=config.CELERY_BROKER_URL,
)


def sync(f):
    @functools.wraps(f)
    def wrapper(*args, **kwargs):
        return asyncio.get_event_loop().run_until_complete(f(*args, **kwargs))

    return wrapper


from workers.tasks import *  # noqa F402 F403

celery_app.conf.task_routes = {"worker.celery_worker.celery": "queue"}
celery_app.conf.update(task_track_started=True)
