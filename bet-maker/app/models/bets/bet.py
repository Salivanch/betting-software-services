import uuid

from sqlalchemy import Boolean, Column, Enum, ForeignKey, Numeric
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import relationship

from modules.database.postgres import Base

from .choices import BetStatus


class Bet(Base):
    __tablename__ = "bets"

    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    status = Column(Enum(BetStatus), default=BetStatus.PENDING)
    amount = Column(Numeric(10, 2), nullable=False)
    odds = Column(Numeric(5, 2), nullable=False)
    is_calculated = Column(Boolean, default=False)
    result = Column(Numeric(5, 2))
    event_id = Column(UUID(as_uuid=True), ForeignKey("events.id"))

    event = relationship("Event", back_populates="bets")
