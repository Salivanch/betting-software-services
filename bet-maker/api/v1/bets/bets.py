from app.controllers import BetController
from app.schemas.requests.base import Filter, OrderEnum
from app.schemas.requests.bets import BetCreateRequest
from app.schemas.response_schemas import PaginationResponse
from app.schemas.responses.bets import BetResponse
from core.factory import Factory
from fastapi import APIRouter, Depends

from modules.utils.parsers import parse_q

bet_router = APIRouter()


@bet_router.get("/", name="Получить список ставок")
async def get_list(
    bet_controller: BetController = Depends(Factory().get_bet_controller),
    q: list[Filter] = Depends(parse_q),
    page: int = 1,
    limit: int = 10,
    sort_by: str = None,
    order: OrderEnum = OrderEnum.asc,
) -> PaginationResponse[BetResponse]:
    results = await bet_controller.get_by(
        page=page,
        limit=limit,
        filter_list=q,
        sort_by=sort_by,
        order=sort_by,
        pydantic_model=BetResponse,
    )
    found = await bet_controller.count()
    return PaginationResponse[BetResponse](
        meta={"found": found, "page": page, "limit": limit}, items=results
    )


@bet_router.post("/", name="Сделать ставку")
async def create(
    bet_create: BetCreateRequest,
    bet_controller: BetController = Depends(Factory().get_bet_controller),
) -> BetResponse:
    new_bet = bet_create.model_dump()
    bet = BetCreateRequest(**new_bet)
    return await bet_controller.make_bet(bet)
