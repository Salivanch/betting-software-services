from core.config import config
from fastapi import Depends
from sqlalchemy import text
from sqlalchemy.ext.asyncio import create_async_engine

from modules.database import get_session


class PostgresService:
    @classmethod
    async def health_check(cls, db_session=Depends(get_session)) -> bool:
        url = config.POSTGRES_URL if config.IS_DOCKER else config.POSTGRES_URL_LOCAL
        engine = create_async_engine(
            url,
        )
        async with engine.connect() as conn:
            result = await conn.execute(text("SELECT 1"))
            return bool(result.scalar())
