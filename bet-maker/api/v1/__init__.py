from fastapi import APIRouter

from .bets import bets_router
from .events import events_router

v1_router = APIRouter()
v1_router.include_router(events_router)
v1_router.include_router(bets_router)
