from collections import defaultdict
from typing import Any, Generic

from pydantic import BaseModel, Field, model_validator

from modules.database.postgres.session import Base
from modules.repository.base import PydanticType

DEFAULT_FIELDS = ["id", "created_at"]


class MetaData(BaseModel):
    found: int
    limit: int
    page: int


class BaseResponse(BaseModel, Generic[PydanticType]):
    allowed_fields: dict[str, list[str]] | None = Field(None, exclude=True)

    model_config = {
        "extra": "allow",
    }

    @classmethod
    def filter_output(
        cls,
        output: dict[str, Any],
        allowed_fields: dict[str, list[str]],
        model_name: str = "items",
    ) -> dict[str, Any]:
        filtered_output = defaultdict(list)
        for field, value in output.items():
            if isinstance(value, dict):
                filtered_output[field] = cls.filter_output(
                    value, allowed_fields, model_name=field
                )
            elif isinstance(value, Base):
                filtered_output[field] = cls.filter_output(
                    value.__dict__, allowed_fields, model_name=field
                )
            elif field in allowed_fields.get(model_name, []) or field in DEFAULT_FIELDS:
                filtered_output[field] = value
        return filtered_output


class PaginationResponse(BaseResponse[PydanticType]):
    meta: MetaData
    items: list[PydanticType]

    @model_validator(mode="after")
    def validate(self):
        if self.allowed_fields is None:
            return self
        allowed_fields = self.allowed_fields
        self.items = [
            self.filter_output(
                item.model_dump() if isinstance(item, BaseModel) else item,
                allowed_fields,
            )
            for item in self.items
        ]
        return self
