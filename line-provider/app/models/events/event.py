import uuid

from sqlalchemy import (Column, DateTime, Enum, ForeignKey, Numeric,
                        UniqueConstraint)
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import relationship

from modules.database.postgres import Base

from .choices import EventStatus


class Event(Base):
    __tablename__ = "events"

    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    status = Column(Enum(EventStatus), default=EventStatus.PENDING)
    match_id = Column(UUID(as_uuid=True), ForeignKey("matches.id"))
    event_type_id = Column(UUID(as_uuid=True), ForeignKey("event_types.id"))
    odds = Column(Numeric(5, 2), nullable=False)
    deadline = Column(DateTime)

    match = relationship("Match", back_populates="events")
    event_type = relationship("EventType", back_populates="events")

    __table_args__ = (
        UniqueConstraint("match_id", "event_type_id", name="match_eventtype_uc"),
    )
