from fastapi import APIRouter

from .events import event_router

events_router = APIRouter()
events_router.include_router(event_router, prefix="/events", tags=["Events"])
