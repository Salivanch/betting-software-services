from functools import partial

import app.controllers as c
import app.models as m
import app.repositories as r
from fastapi import Depends

from modules.database import get_session


class Factory:
    """
    This is the factory container that will instantiate all the controllers and
    repositories which can be accessed by the rest of the application.
    """

    bet_repository = partial(r.BetRepository, m.Bet)
    event_repository = partial(r.EventRepository, m.Event)

    def get_bet_controller(self, db_session=Depends(get_session)) -> c.BetController:
        return c.BetController(
            bet_repository=self.bet_repository(db_session=db_session),
        )

    def get_event_controller(
        self, db_session=Depends(get_session)
    ) -> c.EventController:
        return c.EventController(
            event_repository=self.event_repository(db_session=db_session),
        )
