import logging

from app.models.events import Event
from app.repositories import EventRepository
from app.schemas.requests.events import (EventCreateRequest,
                                         EventUpdateWithIDRequest)
from app.schemas.responses.events import EventResponse
from app.services.kafka import kafka_service

from modules.controller import BaseController

logger = logging.getLogger(__name__)


class EventController(BaseController[Event]):
    def __init__(self, event_repository: EventRepository) -> None:
        super().__init__(model=Event, repository=event_repository)

    async def update_event(
        self, updated_event: EventUpdateWithIDRequest
    ) -> EventResponse:
        event = await self.update(pydantic_model=updated_event)

        event_data = self.model_to_dict(event)
        event_message = EventResponse(**event_data).model_dump_json()

        try:
            await kafka_service.send("update_event", value=event_message)
        except Exception as e:
            logger.error(f"Error sending message to Kafka: {e}")

        return event

    async def create_event(self, created_event: EventCreateRequest) -> EventResponse:
        event = await self.create(pydantic_model=created_event)

        event_data = self.model_to_dict(event)
        event_message = EventResponse(**event_data).model_dump_json()

        try:
            await kafka_service.send("create_event", value=event_message)
        except Exception as e:
            logger.error(f"Error sending message to Kafka: {e}")

        return event
