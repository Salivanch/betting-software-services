from enum import Enum

from pydantic import BaseModel, Field


class OrderEnum(str, Enum):
    asc = "asc"
    desc = "desc"


class Filter(BaseModel):
    field: str = Field(..., description="The field to filter by")
    op: str = Field(..., description="The operation to apply")
    value: str | int = Field(..., description="The value to compare against")


class CommonQueryParams(BaseModel):
    filter_list: str | None = Field(default=None, alias="q")
    page: int = Field(default=1)
    limit: int = Field(default=10)
    sort_by: str | None = None
    order: OrderEnum = OrderEnum.asc
