from app.models.events import Event
from app.models.events.constants import EVENT_TO_CALCULATED
from app.repositories import EventRepository
from app.schemas.requests.events import EventRequest
from workers import calculated_bets

from modules.controller import BaseController


class EventController(BaseController[Event]):
    def __init__(self, event_repository: EventRepository) -> None:
        super().__init__(model=Event, repository=event_repository)

    async def update_event(self, updated_event_data: EventRequest) -> Event:
        event = await self.update(updated_event_data)

        if updated_event_data.status in EVENT_TO_CALCULATED:
            calculated_bets.delay(updated_event_data.id, updated_event_data.status)

        return event
