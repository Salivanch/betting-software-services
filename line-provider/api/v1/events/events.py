from uuid import UUID

from app.controllers import EventController
from app.schemas.requests.base import Filter, OrderEnum
from app.schemas.requests.events import (EventCreateRequest,
                                         EventUpdateRequest,
                                         EventUpdateWithIDRequest)
from app.schemas.response_schemas import PaginationResponse
from app.schemas.responses.events import EventResponse
from core.factory import Factory
from fastapi import APIRouter, Depends

from modules.utils.parsers import parse_q

event_router = APIRouter()


@event_router.get("/", name="Получить список событий")
async def get_list(
    event_controller: EventController = Depends(Factory().get_event_controller),
    q: list[Filter] = Depends(parse_q),
    page: int = 1,
    limit: int = 10,
    sort_by: str = None,
    order: OrderEnum = OrderEnum.asc,
) -> PaginationResponse[EventResponse]:
    results = await event_controller.get_by(
        page=page,
        limit=limit,
        filter_list=q,
        sort_by=sort_by,
        order=sort_by,
        pydantic_model=EventResponse,
    )
    found = await event_controller.count()
    return PaginationResponse[EventResponse](
        meta={"found": found, "page": page, "limit": limit}, items=results
    )


@event_router.put("/{id}/", name="Обновить событие")
async def update(
    id: UUID,
    event_update: EventUpdateRequest,
    event_controller: EventController = Depends(Factory().get_event_controller),
) -> EventResponse:
    event_data = event_update.model_dump()
    event = EventUpdateWithIDRequest(id=id, **event_data)
    return await event_controller.update_event(event)


@event_router.post("/", name="Создать событие")
async def create(
    event_create: EventCreateRequest,
    event_controller: EventController = Depends(Factory().get_event_controller),
) -> EventResponse:
    event_data = event_create.model_dump()
    event = EventCreateRequest(**event_data)
    return await event_controller.create_event(event)
