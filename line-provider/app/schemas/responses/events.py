from uuid import UUID

from app.schemas.requests.events import BaseEventRequest


class EventResponse(BaseEventRequest):
    id: UUID
