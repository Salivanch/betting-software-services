from decimal import Decimal
from uuid import UUID

from app.models.bets.choices import BetStatus
from app.schemas.requests.bets import BetRequest


class BetResponse(BetRequest):
    id: UUID
    status: BetStatus
    is_calculated: bool
    result: Decimal | None
    odds: Decimal
