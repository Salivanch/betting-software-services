from app.controllers.event import EventController
from app.models.events import Event
from app.repositories import EventRepository
from sqlalchemy.ext.asyncio import AsyncSession


async def get_event_controller(session: AsyncSession) -> EventController:
    event_repository = EventRepository(Event, session)
    return EventController(event_repository)
