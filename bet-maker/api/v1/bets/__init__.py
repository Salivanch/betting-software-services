from fastapi import APIRouter

from .bets import bet_router

bets_router = APIRouter()
bets_router.include_router(bet_router, prefix="/bets", tags=["Bets"])
