from app.models.matches import Match

from modules.repository import BaseRepository


class MatchRepository(BaseRepository[Match]):
    pass
