from typing import Final

from .choices import BetStatus

BET_TO_CALCULATED = [BetStatus.PENDING]

EVENT_CLOSED_FOR_BETS: Final[str] = "Событие закрыто для ставок"
BETTING_TIME_EXPIRED: Final[str] = "Время для ставок вышло"
