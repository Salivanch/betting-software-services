from datetime import datetime
from decimal import Decimal

from app.models.events.choices import EventStatus
from pydantic import UUID4, BaseModel


class BaseEventRequest(BaseModel):
    status: EventStatus
    odds: Decimal
    deadline: datetime


class EventCreateRequest(BaseEventRequest):
    match_id: UUID4
    # event_type_id: UUID4


class EventUpdateRequest(BaseModel):
    status: EventStatus | None
    match_id: UUID4 | None
    # event_type_id: UUID4 | None
    odds: Decimal | None
    deadline: datetime | None


class EventUpdateWithIDRequest(EventUpdateRequest):
    id: UUID4
